// Subject: Spongebox
//
// This 3D model is published under CC BY-SA 4.0 by Stefan Schönberger <mail@sniner.net>

/* [Sponge dimensions] */

SPONGE_WIDTH        = 60; //[20:120]
SPONGE_DEPTH        = 85; //[20:120]
SPONGE_HEIGHT       = 30; //[20:60]

/* [Box parameters] */

// Space between sponge and box walls
MARGIN              = 3; //[1:10]

// Radius of the corners
FILLET              = 6; //[1:0.1:10]

// Thickness of the box walls
WALL_THICKNESS      = 2.0; //[1.6:0.1:3.0]

// Size of the grate holes
GRATE_OPENING       = 2.5; //[2.0:0.5:5.0]

// Form of the grate holes
GRATE_OPENING_FORM  = "rectangle"; //[rectangle,round,honeycomb]

// I have no idea what it is called in english ;-) Try it out.
INSERTION_FORM = "guides"; //[guides,wall,none]

// Tolerance of this thingy. See above.
INSERTION_TOLERANCE = 0.2; //[0.1:0.1:0.4]

// Determines the smoothness of round objects. Raise to 96 for printing.
ROUNDNESS           = 24; //[24,48,96]

// Which part to display/print or "demo" to give an idea of the spongebox
PART                = "demo"; //[grate,bottom,top,demo,none]

/* [Hidden] */

DRAINING_RACK_HEIGHT = 10;
INSERTION_DEPTH = 2.4;

module copy_mirror(v=[1,0,0])
{
    children();
    mirror(v=v) children();
}

function _hyperbola(x) = sqrt(1.0 + x*x) - 1.0;
function _hyperbola_curve(n, a, s) = [ for (i = [0 : n]) [1.0/n*i, s*_hyperbola(a/n*i)] ];
function _hyperbola_path(n, a) = _hyperbola_curve(n=floor(n), a=a, s=1.0/_hyperbola(a));
function _hyperbola_polygon(n, a, sx=1.0, sy=1.0, ox=0.0, oy=0.0) = 
    concat([[0, 0]], [ for (p = _hyperbola_path(n, a)) [sx*p[1]+ox, sy*p[0]+oy] ], [ [0, sy], [0, 0] ]);

module hyperbola(w, h, t, a=1.0, n)
{
    segs = max(is_undef(n) ? $fn : n, 4);
    linear_extrude(t)
    polygon(_hyperbola_polygon(n=segs, a=a, sx=w, sy=h, ox=1));
}

// --------------------------------------------------------------------------

module round_cube(width, depth, height, fillet)
{
    linear_extrude(height)
        hull() {
            copy_mirror([0, 1, 0]) 
            copy_mirror()
            translate([width/2-fillet, depth/2-fillet, 0]) circle(r=fillet);
        }
}

module guide(width, height, thickness=1, down=false)
{
    translate(down ? [0, -thickness/2, height] : [0, thickness/2, 0])
    rotate(down ? [-90, 0, 0] : [90, 0, 0])   
    linear_extrude(thickness) hull() copy_mirror() translate([width/2-height, 0, 0])
    intersection() {
        circle(r=height);
        translate([-height-1, 0, 0]) square([2*height+2, height+1], center=false);
    }
}

module half_box(width, depth, height, fillet, thickness)
{
    difference() {
        round_cube(
            width=width,
            depth=depth,
            height=height,
            fillet=fillet
        );
        translate([0, 0, thickness])
        round_cube(
            width=width-2*thickness,
            depth=depth-2*thickness,
            height=height,
            fillet=fillet-thickness
        );
    }
}

module box_walls(width, depth, height, fillet, thickness)
{
    difference() {
        round_cube(
            width=width,
            depth=depth,
            height=height,
            fillet=fillet
        );
        translate([0, 0, -1])
        round_cube(
            width=width-2*thickness,
            depth=depth-2*thickness,
            height=height+2,
            fillet=fillet-thickness
        );
    }
}

module grid(width, depth, dist, center=true)
{
    _w = width;
    _d = is_undef(depth) ? _w : depth;
    _dist = is_undef(dist) ? 1 : dist;
    _n_w = floor(_w/_dist);
    _n_d = floor(_d/_dist);
    _w0 = (_w - _n_w*_dist)/2;
    _d0 = (_d - _n_d*_dist)/2;
    translate(center ? [-_w/2, -_d/2, 0] : [0, 0, 0])
    for (d=[1:_n_d-1]) {
        for (w=[1:_n_w-1]) {
            translate([_w0+w*_dist, _d0+d*_dist, 0]) children();
        }
    }
}

module cutout(width, height, thickness)
{
    translate([0, -thickness/2, 0])
    rotate([-90, 0, 0])
    translate([0, -height-2, 0])
    difference() {
        translate([-width/2, 1, 0])
            cube([width, height+1, thickness]);
        translate([0, 0, -1]) copy_mirror() rotate(90)
            hyperbola(height+1, width/2+0.01, thickness+2);
    }
}

module grate(width, depth, height, fillet, hole, thickness)
{
    difference() {
        half_box(width, depth, height, fillet, thickness);
        grid(width-fillet, depth-fillet, dist=hole+thickness)
            if (GRATE_OPENING_FORM=="round")
                cylinder(d=hole, h=height, center=true);
            else if (GRATE_OPENING_FORM=="honeycomb")
                cylinder(d=hole, h=height, center=true, $fn=6);
            else
                cube([hole, hole, height], center=true);
    }
}

// --------------------------------------------------------------------------

module grate_part(facedown=true)
{
    width = SPONGE_WIDTH+2*(MARGIN+WALL_THICKNESS);
    depth = SPONGE_DEPTH+2*(MARGIN+WALL_THICKNESS);
    height = DRAINING_RACK_HEIGHT+WALL_THICKNESS;
    fillet = max(FILLET, WALL_THICKNESS+0.01);
    translate(facedown ? [0, 0, 0] : [0, 0, height])
    rotate(facedown ? 0 : [180, 0, 0])
    difference() {
        grate(
            width = width,
            depth = depth,
            height = height,
            fillet = fillet,
            hole = GRATE_OPENING,
            thickness = WALL_THICKNESS
        );
        if (true) {
            translate([0, 0, height+0.01]) {
                copy_mirror([0, 1, 0])
                translate([0, depth/2-WALL_THICKNESS/2, 0])
                rotate([180, 0, 0])
                cutout(
                    width = width-2*fillet,
                    height = (height-WALL_THICKNESS)*2/3,
                    thickness = WALL_THICKNESS+2
                );
                copy_mirror([1, 0, 0])
                translate([width/2-WALL_THICKNESS/2, 0, 0])
                rotate([180, 0, 90])
                cutout(
                    width = depth-2*fillet,
                    height = (height-WALL_THICKNESS)*2/3,
                    thickness = WALL_THICKNESS+2
                );
            }
        }
    }
}

module bottom_part()
{
    gap = 2*WALL_THICKNESS;
    width = SPONGE_WIDTH+2*(MARGIN+WALL_THICKNESS+gap);
    depth = SPONGE_DEPTH+2*(MARGIN+WALL_THICKNESS+gap);
    height = max(2*DRAINING_RACK_HEIGHT, SPONGE_HEIGHT/3*2);
    difference() {
        half_box(
            width = width,
            depth = depth,
            height = height,
            fillet = FILLET+gap,
            thickness = WALL_THICKNESS
        );
        translate([0, 0, height-INSERTION_DEPTH]) {
            if (INSERTION_FORM=="guides") {
                copy_mirror([1, 0, 0])
                translate([width/2-WALL_THICKNESS+INSERTION_TOLERANCE/2, 0, -INSERTION_TOLERANCE/2])
                rotate(90)
                guide(
                    width = depth-2*(FILLET+gap),
                    height = INSERTION_DEPTH+INSERTION_TOLERANCE/2+0.01,
                    thickness = WALL_THICKNESS,
                    down = true
                );
                copy_mirror([0, 1, 0])
                translate([0, depth/2-WALL_THICKNESS+INSERTION_TOLERANCE/2, -INSERTION_TOLERANCE/2])
                guide(
                    width = width-2*(FILLET+gap),
                    height = INSERTION_DEPTH+INSERTION_TOLERANCE/2+0.01,
                    thickness = WALL_THICKNESS,
                    down = true
                );
            } else if (INSERTION_FORM=="wall") {
                box_walls(
                    width = width - WALL_THICKNESS + INSERTION_TOLERANCE,
                    depth = depth - WALL_THICKNESS + INSERTION_TOLERANCE,
                    height = INSERTION_DEPTH+1,
                    fillet = FILLET+gap - WALL_THICKNESS/2 + INSERTION_TOLERANCE/2,
                    thickness = WALL_THICKNESS
                );
            }
        }
    }
}

module top_part(facedown=true)
{
    gap = 2*WALL_THICKNESS;
    bottom_height = max(2*DRAINING_RACK_HEIGHT, SPONGE_HEIGHT/3*2);
    grate_height = DRAINING_RACK_HEIGHT+WALL_THICKNESS;
    width = SPONGE_WIDTH+2*(MARGIN+WALL_THICKNESS+gap);
    depth = SPONGE_DEPTH+2*(MARGIN+WALL_THICKNESS+gap);
    height = (SPONGE_HEIGHT+grate_height+2*WALL_THICKNESS+MARGIN) - bottom_height;
    translate(facedown ? [0, 0, 0] : [0, 0, height])
    rotate(facedown ? 0 : [180, 0, 0]) {
        difference() {
            half_box(
                width = width,
                depth = depth,
                height = height+(INSERTION_FORM=="wall" ? INSERTION_DEPTH : 0),
                fillet = FILLET+gap,
                thickness = WALL_THICKNESS
            );
            if (INSERTION_FORM=="wall") {
                translate([0, 0, height+INSERTION_TOLERANCE/2]) 
                box_walls(
                    width = width + WALL_THICKNESS - INSERTION_TOLERANCE,
                    depth = depth + WALL_THICKNESS - INSERTION_TOLERANCE,
                    height = INSERTION_DEPTH+1,
                    fillet = FILLET+gap + WALL_THICKNESS/2 - INSERTION_TOLERANCE/2,
                    thickness = WALL_THICKNESS
                );
            }
        }
        if (INSERTION_FORM=="guides") {
            guide_thickness = WALL_THICKNESS/2 - INSERTION_TOLERANCE/2;
            translate([0, 0, height]) {
                copy_mirror([1, 0, 0])
                translate([width/2-WALL_THICKNESS+guide_thickness/2, 0, 0])
                rotate(90)
                guide(
                    width = depth-2*(FILLET+gap),
                    height = INSERTION_DEPTH,
                    thickness = guide_thickness
                );
                copy_mirror([0, 1, 0])
                translate([0, depth/2-WALL_THICKNESS+guide_thickness/2, 0])
                guide(
                    width = width-2*(FILLET+gap),
                    height = INSERTION_DEPTH,
                    thickness = guide_thickness
                );
            }
        }
    }
}

module sponge()
{
    translate([0, 0, SPONGE_HEIGHT/2])
    cube([SPONGE_WIDTH, SPONGE_DEPTH, SPONGE_HEIGHT], center=true);
}

// --------------------------------------------------------------------------

$fn = ROUNDNESS;

if (PART=="grate") {
    grate_part();
} else if (PART=="bottom") {
    bottom_part();
} else if (PART=="top") {
    top_part();
} else if (PART=="demo") {
    bottom_part();
    translate([0, 0, WALL_THICKNESS]) grate_part(facedown=false);
    %translate([0, 0, DRAINING_RACK_HEIGHT+2*WALL_THICKNESS+0.1]) color("green", 0.2) sponge();
    translate([0, 0, SPONGE_HEIGHT*2]) rotate([20, 0, 0]) top_part(facedown=false);
}