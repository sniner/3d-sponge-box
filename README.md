# Sponge box

In the office I like to wash my dishes by hand, but afterwards I have a damp sponge with which I do not know where to put it. So ... I need a sponge box.

Please note: this is a customizable 3D model. The attached STL files are suitable for the usual kitchen sponges in Germany. But you can adapt the box to almost any dimension. Please use the customizer in Thingiverse or, if it doesn't work, download OpenSCAD and open the scad file.

![](demo.png)

Published on [Thingiverse][1] on 10.10.2020.

License: [CC BY-SA 4.0][2]

[1]: https://www.thingiverse.com/thing:4620357
[2]: https://creativecommons.org/licenses/by-sa/4.0/